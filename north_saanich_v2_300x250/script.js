// HogarthClass -----------------------------------------
var H = {
	// get element by ID
	e:function(id){
		return document.getElementById(id);
	},

	// get param by name
	gpn: function(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	} 
}

// CLICKTAG ---------------------------------------------
function regEvent(elem, eventName, fn) {
    if (typeof elem.addEventListener != "undefined") {
        elem.addEventListener(eventName, fn, false);
        return true;
    } else if (typeof elem.attachEvent != "undefined") {
        elem.attachEvent("on" + eventName, fn);
        return true;
    } else if (typeof elem["on" + eventName] == "function") {
        var existing = elem["on" + eventName];
        elem["on" + eventName] = function() {
            existing();
            fn();
        };
        return true;
    } else {
        try { elem["on" + eventName] = fn; } catch (err) {
            return false; }
        return typeof elem["on" + eventName] == "function";
    }
}
regEvent(document.querySelector("#wrapper"), "click", function() { ADGEAR.html5.clickThrough("clickTAG"); });

// Main Timeline ----------------------------------------
var TL = new TimelineMax({ repeat: 0, repeatDelay: 2 });

// Remove trash on init
TL.add(TweenMax.to(H.e('wrapper'), 0, {opacity: 1}));

TL.add([
	TweenMax.from('#cheetah', 0.15, { scale:0.50, x:-100, y:75, ease:Power0.easeNone }),
	TweenMax.from('#cheetah #tail', 0.15, { x:50, y:-20, transformOrigin:"117px 19px", rotation:-30, ease:Power0.easeNone }),
	TweenMax.from('#cheetah #body', 0.15, { x:5, y: 10, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #frontleg1', 0.15, { x:10, transformOrigin:"25px 25px", rotation:-15,ease:Power0.easeNone }),
	TweenMax.from('#cheetah #leg', 0.15, { x:10,y:5,transformOrigin:"32px 30px", rotation:15,ease:Power0.easeNone }),
	TweenMax.from('#cheetah #backleg2', 0.15, { x:10,y:-10,transformOrigin:"25px 25px",rotation:15,ease:Power0.easeNone }),
]);
TL.add([
	TweenMax.to('#cheetah', 0.15, { scale:1.50, x:200, y:25, ease:Power0.easeNone }),
	TweenMax.to("#cheetah #tail", 0.15, { y:15, transformOrigin:"117px 19px", rotation:30, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #leg', 0.15, { x:10,y:-15,transformOrigin:"32px 30px",rotation:-60,ease:Power0.easeNone }),
	TweenMax.to('#cheetah #backleg2', 0.15, { transformOrigin:"25px 25px", x:-20, y:-25, rotation:-30,ease:Power0.easeNone }),
	TweenMax.to('#cheetah #body', 0.15, { x:5, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #frontleg1', 0.15, { x:10, transformOrigin:"25px 25px", rotation:15,ease:Power0.easeNone }),
	TweenMax.to('#cheetah #frontleg2', 0.15, { x:20, y:-15, rotation:30,ease:Power0.easeNone }),
]);
TL.add([
	TweenMax.from(H.e('txt1'), 0.15, {autoAlpha: 0}),
	TweenMax.to('#cheetah', 0.15, { scale:2.00, x:300,y:0, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #leg', 0.15, { x:20, y:5, rotation:0,ease:Power0.easeNone }),
	TweenMax.to('#cheetah #backleg2', 0.15, { transformOrigin:"25px 25px", x:-20,y:-40, rotation:-15, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #frontleg2', 0.15, { x:20, y:-15, rotation:0, ease:Power0.easeNone }),
	TweenMax.to('#cheetah #frontleg1', 0.15, { x:10, transformOrigin:"25px 25px", rotation:-5,ease:Power0.easeNone }),
	TweenMax.to('#cheetah #body', 0.15, { x:10, ease:Power0.easeNone }),
	TweenMax.to("#cheetah #tail", 0.15, { x:10, transformOrigin:"117px 19px", rotation:0, ease:Power0.easeNone }),
])
TL.add([
	TweenMax.to('#cheetah', 0.2, { scale:2.50, x:400,y:-25, autoAlpha:0, ease:Power0.easeNone }),
	])
/*TL.add([
	TweenMax.to(H.e('cheetah1'), 0.5, { x:950, y:0, ease:Power0.easeNone}),
	
]);*/

TL.add([
	TweenMax.to(H.e('fibre'), 1.0, {x: 300+1100, ease:Power1.easeIn}),
	TweenMax.from(H.e('fibre_bright'), 0.2, {delay:0.5, autoAlpha:0, yoyo:true, repeat: 1, ease:Power1.easeIn}),
	TweenMax.to(H.e('txt1'), 0.1, {delay:0.5, autoAlpha: 0}),
	TweenMax.from(H.e('txt2'), 0.1, {delay:0.5, autoAlpha: 0}),
	TweenMax.from(H.e('legal'), 1, {delay:0.5, autoAlpha:0}),
	TweenMax.from(H.e('cheetah2'), 0.1, {delay:0.5, autoAlpha:0}),
	], '+=2');

TL.add([
	TweenMax.from(H.e('logo'), 0.2, {autoAlpha: 0}),
	TweenMax.to(H.e('gradient'), 0.5, {x: -450, y:-50}),
	TweenMax.from(H.e('cta'), 1, {delay:0.5, autoAlpha:0}),
], "-=0");

